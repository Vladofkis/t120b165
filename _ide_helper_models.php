<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\AnalyzedProfiles
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $screen_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\AnalyzedProfiles whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\AnalyzedProfiles whereScreenName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\AnalyzedProfiles whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\AnalyzedProfiles whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\AnalyzedProfilePosts[] $posts
 * @property string $gender
 * @property string $tweets_analyzed
 * @method static \Illuminate\Database\Query\Builder|\App\AnalyzedProfiles whereGender($value)
 * @method static \Illuminate\Database\Query\Builder|\App\AnalyzedProfiles whereTweetsAnalyzed($value)
 */
	class AnalyzedProfiles extends \Eloquent {}
}

namespace App{
/**
 * App\Sample
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Sample whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Sample whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Sample whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Sample whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Sample whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Sample extends \Eloquent {}
}

namespace App{
/**
 * App\Tokens
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $token
 * @property string $gender
 * @property int $count
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Tokens whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Tokens whereToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Tokens whereGender($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Tokens whereCount($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Tokens whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Tokens whereUpdatedAt($value)
 * @property int $male_count
 * @property int $female_count
 * @method static \Illuminate\Database\Query\Builder|\App\Tokens whereMaleCount($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Tokens whereFemaleCount($value)
 */
	class Tokens extends \Eloquent {}
}

