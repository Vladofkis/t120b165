<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Tokens
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $token
 * @property string $gender
 * @property int $count
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Tokens whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Tokens whereToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Tokens whereGender($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Tokens whereCount($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Tokens whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Tokens whereUpdatedAt($value)
 * @property int $male_count
 * @property int $female_count
 * @method static \Illuminate\Database\Query\Builder|\App\Tokens whereMaleCount($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Tokens whereFemaleCount($value)
 */
class Tokens extends Model
{
    protected $table = 'tokens';

    protected $guarded = [];
}
