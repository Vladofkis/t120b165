<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Sample
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Sample whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Sample whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Sample whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Sample whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Sample whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Sample extends Model
{
    protected $table = 'sample';

    protected $guarded = [];

}
