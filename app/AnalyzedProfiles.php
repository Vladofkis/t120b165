<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\AnalyzedProfiles
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $screen_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\AnalyzedProfiles whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\AnalyzedProfiles whereScreenName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\AnalyzedProfiles whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\AnalyzedProfiles whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\AnalyzedProfilePosts[] $posts
 * @property string $gender
 * @property string $tweets_analyzed
 * @method static \Illuminate\Database\Query\Builder|\App\AnalyzedProfiles whereGender($value)
 * @method static \Illuminate\Database\Query\Builder|\App\AnalyzedProfiles whereTweetsAnalyzed($value)
 */
class AnalyzedProfiles extends Model
{
    protected $table = 'analyzed_profiles';

    protected $guarded = [];

    const GENDER_MALE = 0;
    const GENDER_FEMALE = 10;

}
