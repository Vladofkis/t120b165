<?php

function cdn($asset, $cacheBusting = false) {
    if (!Config::get('app.cdn')) {
        return asset($asset);
    }

    $cdns = Config::get('app.cdn');
    $assetName = basename($asset);

    $assetName = explode("?", $assetName);
    $assetName = $assetName[0];

    // Select the CDN URL based on the extension
    foreach ($cdns as $cdn => $types) {
        if (preg_match('/^.*\.(' . $types . ')$/i', $assetName)) {
            return cdnPath($cdn, $asset, $cacheBusting);
        }
    }

    end($cdns);
    return cdnPath(key($cdns), $asset, $cacheBusting);
}

function cdnPath($cdn, $asset, $cacheBusting = false)
{
    return "//" . rtrim($cdn, "/") . "/" . ltrim($asset, "/") . ($cacheBusting ? '?' . config('sentry.release') : '');
}