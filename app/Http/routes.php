<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


use Illuminate\Support\Facades\Input;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use Thujohn\Twitter\Facades\Twitter;

Route::get('/', 'HomeController@index')->name('index');

Route::get('/learn', 'HomeController@learn')->name('learn');
Route::post('/learn', 'HomeController@learnPost')->name('learn-post');
Route::post('/', 'HomeController@calculate')->name('calculate');
Route::get('/userTimeline', function()
{
    dd(Twitter::getUserTimeline(['screen_name' => 'realdonaldtrump', 'count' => 1000, 'tweet_mode' => 'extended', 'format' => 'array', 'max_id' => '795411058357837825']));
    return Twitter::getUserTimeline(['screen_name' => 'thujohn', 'count' => 1000, 'tweet_mode' => 'extended', 'format' => 'array']);
});

Route::get('/sample/{id}', 'SampleController@get');
Route::get('/sample', 'SampleController@getList');
Route::put('/sample/{id}', 'SampleController@put');
Route::post('/sample', 'SampleController@post');
Route::delete('/sample/{id}', 'SampleController@delete');

Route::get('/index','SampleController@index');

Route::get('twitter/login', ['as' => 'twitter.login', function(){
    $sign_in_twitter = true;
    $force_login = false;

    Twitter::reconfig(['token' => '', 'secret' => '']);
    $token = Twitter::getRequestToken(route('twitter.callback'));

    if (isset($token['oauth_token_secret']))
    {
        $url = Twitter::getAuthorizeURL($token, $sign_in_twitter, $force_login);

        Session::put('oauth_state', 'start');
        Session::put('oauth_request_token', $token['oauth_token']);
        Session::put('oauth_request_token_secret', $token['oauth_token_secret']);

        return Redirect::to($url);
    }

    return Redirect::route('twitter.error');
}]);

Route::get('twitter/callback', ['as' => 'twitter.callback', function() {
    if (Session::has('oauth_request_token'))
    {
        $request_token = [
            'token'  => Session::get('oauth_request_token'),
            'secret' => Session::get('oauth_request_token_secret'),
        ];

        Twitter::reconfig($request_token);

        $oauth_verifier = false;

        if (Input::has('oauth_verifier'))
        {
            $oauth_verifier = Input::get('oauth_verifier');
            // getAccessToken() will reset the token for you
            $token = Twitter::getAccessToken($oauth_verifier);
        }

        if (!isset($token['oauth_token_secret']))
        {
            return Redirect::route('twitter.error');
        }

        $credentials = Twitter::getCredentials();

        if (is_object($credentials) && !isset($credentials->error))
        {
            Session::put('access_token', $token);

            return Redirect::to('/');
        }

        return Redirect::route('twitter.error');
    }
}]);

Route::get('twitter/error', ['as' => 'twitter.error', function(){
    // Something went wrong, add your own error handling here
}]);

Route::get('twitter/logout', ['as' => 'twitter.logout', function(){
    Session::forget('access_token');
    return Redirect::to('/');
}]);

Route::post('oauth/access_token', function() {
    return back()->cookie('access_token', Authorizer::issueAccessToken()['access_token'], 60);
})->name('access_token');

Route::get('access_token', 'HomeController@issueAccessToken')->name('access_token-view');
