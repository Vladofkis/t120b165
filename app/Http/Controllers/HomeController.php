<?php

namespace App\Http\Controllers;

use App\AnalyzedProfilePosts;
use App\AnalyzedProfiles;
use App\Http\Requests;
use App\Tokens;
use Illuminate\Http\Request;
use Session;
use Thujohn\Twitter\Facades\Twitter;

class HomeController extends Controller
{
    public function index()
    {
        $access_token = Session::get('access_token');
        return view('home', compact('access_token'));
    }

    public function learn()
    {
        $analyzedProfiles = AnalyzedProfiles::count();
        $analyzedPosts=0;
        $query = AnalyzedProfiles::query();
        $query->each(function (AnalyzedProfiles $profile)  use (&$analyzedPosts) {
            $analyzedPosts+=(int)$profile->tweets_analyzed;
        });
        return view('learn', compact('analyzedProfiles','analyzedPosts'));
    }

    public function calculate(Request $request) {
        if ($request->get('true') || $request->get('false')) {
            $errors = [];
            if (($request->get('true') && $request->get('gender') == AnalyzedProfiles::GENDER_MALE)
                || ($request->get('false') && $request->get('gender') == AnalyzedProfiles::GENDER_FEMALE)) {
                $this->analyzeData([$request->get('name')], AnalyzedProfiles::GENDER_MALE, $errors);
            } else {
                $this->analyzeData([$request->get('name')], AnalyzedProfiles::GENDER_FEMALE, $errors);
            }
            return redirect()->route('index');
        };
        $this->validate($request,[
           'name' => 'required'
        ]);
        $maleCount = Tokens::sum('male_count');
        $femaleCount = Tokens::sum('female_count');
        $maleTokens = Tokens::pluck('male_count', 'token');
        $femaleTokens = Tokens::pluck('female_count', 'token');
        $posts = Twitter::getUserTimeline([
            'screen_name' => $request->get('name'),
            'count' => 200,
            'tweet_mode' => 'extended',
            'format' => 'array'
        ]);
        $finalResults = [];
        $probabilities['zeroes'] = [];
        $probabilities['zeroes-less-drastic'] = [];
        $probabilities['no-zeroes'] = [];
        $probabilities['zeroes-20'] = [];
        $probabilities['no-zeroes-20'] = [];
        foreach ($posts as $post) {
            foreach (explode(" ", $this->formatText($post['full_text'])) as $token) {
                if (!$token || !isset($maleTokens[$token])) {
                    continue;
                }
                if ($maleTokens[$token] == 0) {
                    $probabilities['zeroes'][] = 0.01;
                    $probabilities['zeroes-less-drastic'][] = 0.2;
                    if ($femaleTokens[$token] > 20) {
                        $probabilities['zeroes-20'][] = 0.01;
                    }
                } else if ($femaleTokens[$token] == 0) {
                    $probabilities['zeroes'][] = 0.99;
                    $probabilities['zeroes-less-drastic'][] = 0.8;
                    if ($maleTokens[$token] > 20) {
                        $probabilities['zeroes-20'][] = 0.99;
                    }
                } else {
                    $probability = ($maleTokens[$token]/$maleCount)/(($maleTokens[$token]/$maleCount)+($femaleTokens[$token]/$femaleCount));
                    $probabilities['no-zeroes'][] = $probability;
                    $probabilities['zeroes'][] = $probability;
                    $probabilities['zeroes-20'][] = $probability;
                    $probabilities['zeroes-less-drastic'][] = $probability;
                    if ($maleTokens[$token] > 20 && $femaleTokens[$token] > 20) {
                        $probabilities['no-zeroes-20'][] = $probability;
                    }
                }
            }
        }
        $voters['10'] = 0;
        $voters['20'] = 0;
        $voters['50'] = 0;
        $voters['100'] = 0;
        foreach ($probabilities as $key => $probability) {
            uasort($probability,  function ($a, $b) {
                return (abs($a - 0.5) < abs($b - 0.5)) ? 1 : -1;
            });
            $finalResults["10"][$key] = $this->calculateWithDifferentN($probability,10);
            $finalResults["10"][$key] > 0.5 ? $voters["10"]++ : $voters["10"]--;
            $finalResults["20"][$key] = $this->calculateWithDifferentN($probability,20);
            $finalResults["20"][$key] > 0.5 ? $voters["20"]++ : $voters["20"]--;
            $finalResults["50"][$key] = $this->calculateWithDifferentN($probability,50);
            $finalResults["50"][$key] > 0.5 ? $voters["50"]++ : $voters["50"]--;
            $finalResults["100"][$key] = $this->calculateWithDifferentN($probability,100);
            $finalResults["100"][$key] > 0.5 ? $voters["100"]++ : $voters["100"]--;
        }

        //$otherMethod = number_format(array_sum($slicedProbabilities)/count($slicedProbabilities),4);
        if (!AnalyzedProfiles::where('screen_name', $request->get('name'))->exists()) {
            $name = $request->get('name');
        }
        return view('home', compact('finalResults', 'name', 'voters'));
    }

    public function calculateWithDifferentN($probabilities, $n) {
        $slicedProbabilities = array_slice($probabilities, 0, $n);
        $positive = 1;
        $negative = 1;
        foreach($slicedProbabilities as $slicedProbability) {
            $positive = $positive * $slicedProbability;
            $negative = $negative * (1 - $slicedProbability);
        }
        $results = number_format($positive/($positive+$negative),4);
        if($results == 1) {
            $results = 0.9999;
        } elseif ($results == 0) {
            $results = 0.0001;
        }
        return $results;
    }

    public function learnPost(Request $request)
    {
        $errors = [];

        $males = explode("\r\n", $request->get('males'));
        $females = explode("\r\n", $request->get('females'));
        if (count($males) + count($females) > 6) {
            return back()->withErrors(['Maximum amount of profiles is 6']);
        }

        $this->analyzeData($males, AnalyzedProfiles::GENDER_MALE, $errors);
        $this->analyzeData($females, AnalyzedProfiles::GENDER_FEMALE, $errors);
        return back()->withErrors($errors);
    }

    public function analyzeData($screenNames, $gender, &$errors) {
        foreach ($screenNames as $screenName) {
            $callData = [
                'screen_name' => $screenName,
                'count' => 200,
                'tweet_mode' => 'extended',
                'format' => 'array'
            ];
            if (!$screenName) {
                continue;
            }
            if (AnalyzedProfiles::where('screen_name', $screenName)->exists()) {
                $errors[] = 'User '.$screenName.' already analyzed or invalid screen name!';
                continue;
            }
            $profile = AnalyzedProfiles::create([
                'screen_name' =>  $screenName,
                'gender' => $gender
            ]);
            $counter=0;
            do {
                $posts = Twitter::getUserTimeline($callData);
                foreach ($posts as $post) {
                    $counter++;
                    foreach (explode(" ", $this->formatText($post['full_text'])) as $token) {
                        if (!$token) {
                            continue;
                        }
                        if ($dbtoken = Tokens::where('token',$token)->first()) {
                            if ($gender == AnalyzedProfiles::GENDER_FEMALE) {
                                $dbtoken->increment('female_count');
                            } else {
                                $dbtoken->increment('male_count');
                            }
                        } else {
                            if ($gender == AnalyzedProfiles::GENDER_FEMALE) {
                                $data = [
                                    'male_count' => 0,
                                    'female_count' => 1,
                                    'token' => $token
                                ];
                            } else {
                                $data = [
                                    'male_count' => 1,
                                    'female_count' => 0,
                                    'token' => $token
                                ];
                            }
                            Tokens::create($data);
                        }
                    }
                }
                $maxId = end($posts)['id'];
                $maxId = $this->substractFromString($maxId);
                $callData['max_id'] = $maxId;
                if ($counter > 900) {
                    $posts=[];
                }
            }
            while (!empty($posts));
            $profile->update([
               'tweets_analyzed' => $counter
            ]);
        }
        return true;

    }

    public function issueAccessToken()
    {
       return view('access-token');
    }

    public function substractFromString($string) {
        return substr($string, 0, -8).strval(((int)substr($string, -8))-1);
    }

    public function formatText($text) {
        $pattern = "/[a-zA-Z]*[:\/\/]*[A-Za-z0-9\-_]+\.+[A-Za-z0-9\.\/%&=\?\-_]+/i";
        $replacement = "";
        return preg_replace($pattern, $replacement, $text);
    }

}
