<?php

namespace App\Http\Controllers;

use App\Sample;
use Cookie;
use Illuminate\Http\Request;
use League\OAuth2\Server\Grant\ClientCredentialsGrant;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use Mockery\Exception;

class SampleController extends Controller
{

    public function index()
    {
        return view('sample');
    }

    public function get($id)
    {
        return response(json_encode(Sample::whereId($id)->first()->toArray()));
    }

    public function getList()
    {
        return response(json_encode(Sample::select(['id', 'name', 'created_at'])->get()->toArray()));
    }

    public function post(Request $request)
    {
        $accessToken = Cookie::get('access_token', '') ?: '';
        if (!Authorizer::validateAccessToken(false, $accessToken)) {
            return response('Invalid Access Token', 401);
        }
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required'
        ]);
        $sample = Sample::create([
            'name' => $request->get('name'),
            'description' => $request->get('description')
        ]);
        return response(json_encode($sample), 201);
    }

    public function put(Request $request, $id)
    {
        $accessToken = Cookie::get('access_token', '') ?: '';
        if (!Authorizer::validateAccessToken(false, $accessToken)) {
            return response('Invalid Access Token', 401);
        }
        $sample = Sample::whereId($id)->update([
            'name' => $request->get('name'),
            'description' => $request->get('description')
        ]);
        return response(json_encode($sample));
    }

    public function delete($id)
    {
        $accessToken = Cookie::get('access_token', '') ?: '';
        if (!Authorizer::validateAccessToken(false, $accessToken)) {
            return response('Invalid Access Token', 401);
        }
        Sample::whereId($id)->delete();
        return response('');
    }
}
