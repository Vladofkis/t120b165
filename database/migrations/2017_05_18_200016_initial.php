<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Initial extends Migration
{

    public function up()
    {
        Schema::create('analyzed_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('screen_name',191);
            $table->tinyInteger('gender');
            $table->integer('tweets_analyzed');
            $table->timestamps();
        });
        Schema::create('tokens', function (Blueprint $table) {
            $table->increments('id');
            $table->string('token',191);
            $table->integer('male_count')->unsigned()->default(0);
            $table->integer('female_count')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('analyzed_profiles');
        Schema::drop('tokens');
    }
}
