<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SampleMigration extends Migration
{
    public function up()
    {
        Schema::create('sample', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',191);
            $table->text('description');
            $table->timestamps();
        });
    }

    public function down()
    {

    }
}
