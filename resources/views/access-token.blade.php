@extends('layout.master')

@section('content')
    <div class="jumbotron">
        @if($accessToken = Cookie::get('access_token'))
            <div class="alert alert-dismissible alert-success col-lg-12" style="padding:15px 35px">
                <h2>Your access token is <b>{{$accessToken}}</b></h2>
                <p>Token has been saved into your browser cookies, you don't need to do anything else</p>
            </div>

        @else
            <h1>Get Client Access Token</h1>
            <p>Enter your Client Secret and get an access token</p>
            <form method="post" action="{{route('access_token')}}">
                <input type="hidden" name="grant_type" value="client_credentials">
                <input type="hidden" name="client_id" value="1">
                <div class="form-group">
                    <label for="client_secret">Client Secret</label>
                    <input id="client_secret" class="form-control" name="client_secret" value="" style="display:block">
                </div>
                <button class="btn btn-block btn-lg btn-success" type="submit">Submit</button>
            </form>
        @endif
    </div>
@endsection