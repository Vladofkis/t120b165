@extends('layout.master')

@section('content')
    {!! Form::open(['route' => 'calculate']) !!}
        <div align="center">
            <div class="jumbotron">
                <div class="row">
                    <h1>Twitter Gender Classificator <img src="{{URL::asset('/images/genders.png')}}" style="width:150px"/></h1>
                    <p>
                        This classificator calculates the probability of a Twitter users male or female gender based on the persons Twitter posts.
                        All you have to do is copy a <b>Screen name</b> of the user you wish to check.
                    </p>
                    @if(isset($access_token) && $access_token)
                        <p>
                            <b>Hello {{Twitter::getCredentials()->screen_name}}</b>
                            <div class="row" style="margin-bottom:20px">
                                <div class="col-lg-12">
                                    <a class="btn btn-danger btn-xs" href="http://localhost:8000/twitter/logout">Logout</a>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                {{ Form::text('name', isset($name) ? $name : '', ['class' => 'form-control', 'placeholder' => 'User screen name here.']) }}
                            </div>
                            <div class="col-lg-6">
                                {{ Form::submit('Check!', ['class' => 'btn btn-primary btn-block']) }}
                            </div>
                        </p>
                    @else
                        <div class="col-lg-12">
                            <a class="btn btn-primary btn-block" href="http://localhost:8000/twitter/login">Authenticate with Twitter</a>
                        </div>
                    @endif
                </div>
                @if (isset($finalResults))
                    @foreach($finalResults as $key => $finalResult)
                        <div align="center">
                            <h2>Calculated via Bayas classification N = {{$key}}</h2>
                            @foreach($finalResult as $result)
                                <div class="col-md-6 col-xs-12">
                                    <div class="row margin-top-15">
                                        <div class="gender {{$result < 0.5 ? 'female' : 'male'}}">
                                            {{$result < 0.5 ? 100-($result*100) : $result*100}}%
                                            <img src="{{$result < 0.5 ? URL::asset('/images/female.png') : URL::asset('/images/male.png')}}"/>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div style="clear:both"></div>
                        <h1>
                            CONCLUSION:
                            <div class="gender {{$voters[$key] < 0 ? 'female' : 'male'}}">
                                {{$voters[$key] < 0 ? "Female" : "Male"}}
                                <img src="{{$voters[$key] < 0 ? URL::asset('/images/female.png') : URL::asset('/images/male.png')}}" />
                            </div>
                        </h1>
                        @if (isset($name) && $name)
                            <h2>Help us improve! Was the Bayas classification correct?</h2>
                            {{Form::hidden('gender', $voters[$key] < 0 ? \App\AnalyzedProfiles::GENDER_FEMALE : \App\AnalyzedProfiles::GENDER_MALE)}}
                            <div class="row">
                                <div class="col-lg-6">
                                    <button type="submit" name="true" value="1" class="btn btn-success btn-block">Correct!</button>
                                </div>
                                <div class="col-lg-6">
                                    <button type="submit" name="false" value="1" class="btn btn-danger btn-block">Incorrect!</button>
                                </div>
                            </div>
                        @endif
                    @endforeach
                @endif
            </div>
        </div>
    {!! Form::close() !!}
@endsection
