@extends('layout.master')

@section('content')
    <div class="jumbotron">
        <h1>Sample Data</h1>
        <p>Press F12 to open console and view the output</p>
        <div ng-app="sampleApp" ng-controller="SampleController" ng-cloak>
            <div class="row">
                <div class="col-md-12">
                    <button type="button" class="btn btn-xs btn-default" ng-click="clearSampleSelection()">CLEAR SELECTION</button>
                </div>
            </div>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th style="width:50%">Name</th>
                        <th>Created At</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="sample in samples" ng-class="sample.id == selectedSample.id ? 'info' : ''">
                        <td style="cursor:pointer; width:50%" ng-click="selectSample(sample.id)">@{{sample.name}}</td>
                        <td style="cursor:pointer" ng-click="selectSample(sample.id)">@{{sample.created_at}}</td>
                        <td>
                            <button type="button" class="btn btn-xs btn-warning" ng-show="sample.id != selectedSample.id" ng-click="deleteSample(sample.id)">DELETE SAMPLE</button>
                        </td>
                    </tr>
                </tbody>
            </table>
            <hr />
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input class="form-control" id="name" ng-model="selectedSample.name" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name">Description</label>
                        <textarea class="form-control" rows=5 id="name" ng-model="selectedSample.description"> </textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" ng-show="!selectedSample.id">
                    <button type="button" class="btn btn-success btn-block" ng-click="createSample(selectedSample)">CREATE SAMPLE</button>
                </div>
                <div class="col-md-12" ng-show="selectedSample.id">
                    <button type="button" class="btn btn-primary btn-block" ng-click="updateSample(selectedSample)">UPDATE SAMPLE</button>
                </div>
            </div>
        </div>
        <div align="center">
            <img src="{{URL::asset('/images/homer-simpson.svg')}}">
        </div>
    </div>
@endsection