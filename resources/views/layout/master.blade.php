<!DOCTYPE html>

<html>
<head>
    <title>Lyties Klasifikatorius</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <script src="http://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
    <link href="http://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>

    <link href="{{ URL::asset('/css/bootstrap-cosmo.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('/css/Site.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('/scripts/jquery-ui/jquery-ui.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('/fonts/bootstrap/glyphicons-halflings-regular.woff2') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet"  href="https://fonts.googleapis.com/css?family=Tangerine">
    <script src={{ URL::asset('/scripts/jquery/jquery.js') }}></script>
    <script src={{ URL::asset('/scripts/jquery-ui/jquery-ui.js') }}></script>
    <script src={{ URL::asset('/scripts/jquery-ui/lithuanian-datepicker.js') }}></script>
    <script src={{ URL::asset('/scripts/bootstrap/bootstrap.js') }}></script>
    <script src="{{cdn('bower/angular/angular.min.js')}}"></script>
    <script src="{{cdn('scripts/sample.js')}}"></script>
    <script>
        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for(var i = 0; i <ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) === ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) === 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
    </script>
</head>
<body>
<div class="container">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Twitter Gender Guess</a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li><a href="/index">Sample</a></li>
                    <li><a href="/access_token">Access Token</a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>
<header>
    <h3 class="credentials">Vladislovas Kofyrinas 2017 <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal">More Information</button></h3>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Author information</h4>
                </div>
                <div class="modal-body" align="center">
                    <p>
                        This application was created by <b>Vladislovas Kofyrinas</b> 2017 <br>
                        KTU Senior year student from IFF-4/3 group.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</header>
<div class="container">
    @if (count($errors) > 0)
        <div class="alert alert-danger margin-top-15">
            @foreach ($errors->all() as $error)
                <div>{!! $error !!} </div>
            @endforeach
        </div>
    @endif
    @yield('content')
</div>
<footer>
    <h3 class="credentials">Vladislovas Kofyrinas 2017</h3>
</footer>
</body>
</html>
