@extends('layout.master')

@section('content')
    {!! Form::open(['route' => 'learn-post']) !!}
        <div align="center">
            <div class="jumbotron">
                <div class="row margin-top-15">
                    <div class="alert alert-dismissible alert-success col-lg-12" style="padding:15px 35px">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <h2>So far we've analyzed...</h2>
                        <p>
                            <b>{{$analyzedProfiles}}</b> Twitter profile(s) <br/>
                            <b>{{$analyzedPosts}}</b> Tweet(s) <br/>
                        </p>
                    </div>
                </div>
                <h1>Learning data for the classificator</h1>
                <p>Specify <b>Male</b> and <b>Female</b> Twitter Screen names (each Screen name on a new line, maximum <b>6</b> profiles at a time).</p>
                <div class="row" align="left">
                    <div class="col-lg-6">
                        {{ Form::label('males', 'Male gender profile Screen names.')}}
                        {{ Form::textarea('males', '', ['class' => 'form-control', 'placeholder' => 'Male Screen names here.', 'rows' => '3']) }}
                    </div>
                    <div class="col-lg-6">
                        {{ Form::label('females', 'Female gender profile Screen names.')}}
                        {{ Form::textarea('females', '', ['class' => 'form-control', 'placeholder' => 'Female Screen names here.', 'rows' => '3']) }}
                    </div>
                </div>
                <div class="row margin-top-15">
                    <div class="col-lg-12">
                        {{Form::submit('Learn', ['class' => 'btn btn-default btn-block'])}}
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}

@endsection
