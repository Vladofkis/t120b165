var app = angular.module('sampleApp', []);
app.controller("SampleController", ['sampleFactory','$scope', function(sampleFactory, $scope) {
    $scope.selectedSample = {};
    $scope.samples = [];
    $scope.loadingList = true;
    reloadSamples();
    function reloadSamples(){
        sampleFactory.getSampleList().then(function(response) {
            $scope.samples = response;
            $scope.loadingList = false;
        });
    }

    $scope.selectSample = function(id) {
        sampleFactory.getSample(id).then(function(response) {
            $scope.selectedSample = response;
            $scope.loadingList = false;

        });
    };
    $scope.clearSampleSelection = function() {
        $scope.selectedSample = {};
    };

    $scope.createSample = function() {
        sampleFactory.postSample($scope.selectedSample).then(function(response) {
            $scope.selectedSample = {};
            reloadSamples();
        })
    };

    $scope.deleteSample = function(id) {
        sampleFactory.deleteSample(id).then(function(response) {
            reloadSamples();
        })
    };

    $scope.updateSample = function() {
        sampleFactory.putSample($scope.selectedSample).then(function(response) {
            $scope.selectedSample = {};
            reloadSamples();
        })
    };
}]);

app.factory('sampleFactory',['$http', function($http){
    return {
        getSampleList : function() {
            return  $http.get('http://localhost:8000/sample').then(
                function(response){
                    return response.data;
                },
                function(response){
                    alert("Server error getting samples failed");
                    return [];
                });
        },
        getSample : function(id) {
            return  $http.get('http://localhost:8000/sample/' + id).then(
                function(response){
                    return response.data;
                },
                function(response){
                    alert("Couldn't get data for sample");
                    return [];
                });
        },
        postSample : function(sample) {
            return  $http.post('http://localhost:8000/sample/', sample).then(
                function(response){
                    return response.data;
                },
                function(response) {

                    alert("Creation failed invalid");
                    return [];
                });
        },
        deleteSample : function(id) {
            return  $http.delete('http://localhost:8000/sample/' + id).then(
                function(response){
                    return response.data;
                },
                function(response){
                    alert("Deletion failed.");
                    return [];
                });
        },
        putSample : function(sample) {
            return  $http.put('http://localhost:8000/sample/' + sample.id, sample).then(
                function(response){
                    return response.data;
                },
                function(response){
                    alert("Update failed.");
                    return [];
                });
        }
    }
}]);